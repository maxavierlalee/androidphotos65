package com.example.photoalbumandroid;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import model.Account;
import model.Album;

public class HomeActivity extends AppCompatActivity {

    public static final String ACCOUNT_TEXT = "com.example.photoalbumandroid.ACCOUNT_TEXT";
    public static final String ACCOUNT_ALBUM = "com.example.photoalbumandroid.ALBUM_TEXT";

    public static Account acc;
    public AlbumAdapter albumAdapter;
    public static int selectedAlbumIndex;

    ListView albumListView;

    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        acc = Account.getSavedData(context);

        if (acc == null) {
            acc = new Account("Account");
        }
        if (acc.albums == null) {
            acc.albums = new ArrayList<Album>();
        }

        Button openButton = (Button) findViewById(R.id.openButton);
        Button createButton = (Button) findViewById(R.id.createButton);
        Button deleteButton = (Button) findViewById(R.id.deleteButton);
        Button renameButton = (Button) findViewById(R.id.renameButton);
        Button searchButton = (Button) findViewById(R.id.moveButton);

        albumAdapter = new AlbumAdapter(this, acc.albums);
        albumListView = (ListView) findViewById(R.id.albumListView);
        albumListView.setAdapter(albumAdapter);

        albumListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int index, long id) {
                Album al = (Album)parent.getItemAtPosition(index);
                selectedAlbumIndex = index;
                String value = al.getName();
                Toast.makeText(HomeActivity.this, value, Toast.LENGTH_SHORT).show();
            }
        });

        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedAlbumIndex > -1) {
                    try{
                        Intent intent = new Intent(getApplicationContext(), AlbumViewActivity.class);
                        intent.putExtra(ACCOUNT_TEXT, acc);
                        intent.putExtra(ACCOUNT_ALBUM, selectedAlbumIndex);
                        startActivity(intent);
                    } catch (Exception e) { Toast.makeText(HomeActivity.this, "Cannot Open", Toast.LENGTH_SHORT).show();}
                }else{
                    Toast.makeText(HomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    try{
                        Intent intent = new Intent(getApplicationContext(), SearchPhotosActivity.class);
                        startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(HomeActivity.this, "Cannot Open", Toast.LENGTH_SHORT).show();
                        Toast.makeText(HomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
        });

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText txtUrl = new EditText(context);

                txtUrl.setHint("Enter Album Name...");

                AlertDialog alertDialog = new AlertDialog.Builder(context)
                        .setTitle("Create a New Album")
                        .setView(txtUrl)
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int choice) {
                                String nameText = txtUrl.getText().toString();
                                if (nameText.isEmpty())
                                    return;

                                Album al = new Album(nameText);
                                if(acc.albumExists(al.getName())) {
                                    Toast.makeText(HomeActivity.this, "Unable to Add!", Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                Toast.makeText(HomeActivity.this, "Added to List!", Toast.LENGTH_SHORT).show();
                                acc.addAlbum(al);
                                acc.updateInfo(context);
                                updateList();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int choice) {
                                Toast.makeText(HomeActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(selectedAlbumIndex);
                if (selectedAlbumIndex > -1) {
                    try {
                        Album al = acc.albums.get(selectedAlbumIndex);
                        acc.deleteAlbum(al);
                        Toast.makeText(HomeActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) { Toast.makeText(HomeActivity.this, "Cannot Delete", Toast.LENGTH_SHORT).show();}
                } else {
                    Toast.makeText(HomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    return;
                }
                acc.updateInfo(context);
                updateList();
            }
        });

        renameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //position = album_list_view.getCheckedItemPosition();
                if (selectedAlbumIndex > -1) {
                    try {
                        final EditText txtUrl = new EditText(context);
                        txtUrl.setHint("Enter new name");

                        AlertDialog alertDialog = new AlertDialog.Builder(context)
                                .setTitle("Rename")
                                .setMessage("for album: " + acc.getAlbums().get(selectedAlbumIndex).getName())
                                .setView(txtUrl)
                                .setPositiveButton("Enter", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        String newName = txtUrl.getText().toString();
                                        if (newName.isEmpty()) {
                                            Toast.makeText(HomeActivity.this, "Invalid", Toast.LENGTH_SHORT).show();
                                            return;
                                        } else if (acc.albumExists(newName)){
                                            Toast.makeText(HomeActivity.this, "Already exists", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        Album al = acc.getAlbums().get(selectedAlbumIndex);
                                        al.setName(newName);

                                        Toast.makeText(HomeActivity.this, "Renamed", Toast.LENGTH_SHORT).show();
                                        acc.updateInfo(context);
                                        updateList();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        Toast.makeText(HomeActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .show();
                    } catch (Exception e) {
                        Toast.makeText(HomeActivity.this, "Cannot Open", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    Toast.makeText(HomeActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
    }



    public void updateList(){
        albumAdapter.notifyDataSetChanged();
        albumListView.setAdapter(albumAdapter);
    }
}
