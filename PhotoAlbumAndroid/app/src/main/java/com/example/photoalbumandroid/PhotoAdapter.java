package com.example.photoalbumandroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import model.Photo;

public class PhotoAdapter extends ArrayAdapter<Photo> {
    Context context;
    public PhotoAdapter(Context context, ArrayList<Photo> photo){
        super(context, R.layout.list_photos, photo);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater albumInflater = LayoutInflater.from(getContext());
        View customView = albumInflater.inflate(R.layout.list_photos, parent, false);

        String photoTitle = getItem(position).title;
        String photoPath = getItem(position).path;

        TextView photoText = customView.findViewById(R.id.photoText);
        ImageView photoImage = customView.findViewById(R.id.photoImage);
        photoText.setText(photoTitle);
        int draw = context.getResources().getIdentifier(photoPath, "drawable", context.getPackageName());
        photoImage.setImageResource(draw);

        return customView;
    }
}