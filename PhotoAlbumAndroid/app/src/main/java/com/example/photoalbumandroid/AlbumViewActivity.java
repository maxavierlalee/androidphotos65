package com.example.photoalbumandroid;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import model.Account;
import model.Album;
import model.Photo;

public class AlbumViewActivity extends AppCompatActivity {

    public Account acc;
    public Album selectedAl;
    public Album desiredAl;
    public PhotoAdapter photoAdapter;
    public ListView photoListView;
    public boolean alExists;

    public static int selectedIndex;

    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_view);

        this.acc = HomeActivity.acc;

        if (acc == null) {
            acc = new Account("Account");
        }

        Intent intent = getIntent();
        int albumIndex = intent.getIntExtra(HomeActivity.ACCOUNT_ALBUM, 0);
        selectedAl = acc.albums.get(albumIndex);

        acc = Account.getSavedData(context);

        if (selectedAl.photos == null) {
            selectedAl.photos = new ArrayList<Photo>();
        }

        System.out.println("gettem");
        System.out.println(selectedAl.getName());
        System.out.println(selectedAl.getName());

        System.out.println("On create: "+selectedAl.getPhotos().toString());


        Button openButton = (Button) findViewById(R.id.openButton);
        Button addButton = (Button) findViewById(R.id.addButton);
        Button deleteButton = (Button) findViewById(R.id.deleteButton);
        Button recaptionButton = (Button) findViewById(R.id.recaptionButton);
        Button moveButton = (Button) findViewById(R.id.moveButton);
        Button backButton = (Button) findViewById(R.id.backButton);

        photoAdapter = new PhotoAdapter(this, selectedAl.photos);
        photoListView = (ListView) findViewById(R.id.photoListView);
        photoListView.setAdapter(photoAdapter);

        photoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int index, long id) {
                Photo photo = (Photo)parent.getItemAtPosition(index);
                selectedIndex = index;
                String value = photo.getTitle();
                Toast.makeText(AlbumViewActivity.this, value, Toast.LENGTH_SHORT).show();
            }
        });

        //System.out.println(selectedAl.photos.get(0).getTitle());

        //adds photo to album
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);

                final EditText photo_name = new EditText(context);
                final EditText photo_path = new EditText(context);
                photo_name.setHint("Enter photo title here");
                photo_path.setHint("Enter file name without file signature");

                layout.addView(photo_name);
                layout.addView(photo_path);

                AlertDialog alertDialog = new AlertDialog.Builder(context)
                        .setMessage("Enter Photo Info to be Added")
                        .setView(layout)
                        .setPositiveButton("Enter", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String result_name = photo_name.getText().toString();
                                String result_path = photo_path.getText().toString();
                                if (result_name.isEmpty() || result_path.isEmpty())
                                    return;


                                Boolean exists = false;
                                Photo p = new Photo(result_name, result_path);
                                for(Photo ph : selectedAl.photos){
                                    if(ph.equals(p)){
                                        exists = true;
                                    } else{
                                        exists = false;
                                    }
                                }
                                if(!exists){
                                    selectedAl.addPhoto(p);
                                    Toast.makeText(AlbumViewActivity.this, "Added", Toast.LENGTH_SHORT).show();
                                    HomeActivity.acc.updateInfo(context);
                                    updateList();
                                }

                        }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Toast.makeText(AlbumViewActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            }
        });

        // opens photo to the photoView screen
        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("open buttton detected");
                try{
                    Intent intent = new Intent(getApplicationContext(), PhotoViewActivity.class);
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(AlbumViewActivity.this, "Cannot Open", Toast.LENGTH_SHORT).show();
                    Toast.makeText(AlbumViewActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

        // removes photo from album
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if (selectedIndex > -1) {
                    try {
                        Photo p = selectedAl.getPhotos().get(selectedIndex);
                        if(p != null && selectedAl.photoExists(p.getTitle()))
                            Toast.makeText(AlbumViewActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                            selectedAl.deletePhoto(p);

                    } catch (Exception e) { Toast.makeText(AlbumViewActivity.this, "Cannot Delete", Toast.LENGTH_SHORT).show();return;}
                } else {
                    Toast.makeText(AlbumViewActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    return;
                }
                selectedIndex = -1;
                HomeActivity.acc.updateInfo(context);
                updateList();
            }
        });

        // moves selected photo do a newly desired already existing album
        moveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);

                final EditText album_name = new EditText(context);
                album_name.setHint("Enter name album to be moved to");

                layout.addView(album_name);

                AlertDialog alertDialog = new AlertDialog.Builder(context)
                        .setMessage("Enter Photo Information")
                        .setView(layout)
                        .setPositiveButton("Enter", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String result_name = album_name.getText().toString();
                                if (result_name.isEmpty())
                                    return;

                                for (Album al: acc.albums) {
                                    if (al.getName().equalsIgnoreCase(result_name)) {

                                        Photo p = selectedAl.photos.get(selectedIndex);
                                        if(!al.photoExists(p.getTitle())){
                                            al.addPhoto(p);
                                            System.out.print(al.getName() + "dskjfhaksldjfhlaksjhdfa");
                                            System.out.println(al.getPhotos().toString());
                                            selectedAl.deletePhoto(p);
                                            HomeActivity.acc.updateInfo(context);
                                            updateList();
                                            alExists = true;
                                        } else{
                                            Toast.makeText(AlbumViewActivity.this, "Photo Already Exists", Toast.LENGTH_SHORT).show();
                                        }
                                        break;
                                    }
                                    alExists = false;
                                }
                                if(alExists==false){
                                    Toast.makeText(AlbumViewActivity.this, "Album Doesn't Exist", Toast.LENGTH_SHORT).show();
                                }



                                for(Album al : acc.getAlbums()){
                                    System.out.println("New albums: "+al.getPhotos().toString());
                                }

                                Toast.makeText(AlbumViewActivity.this, "Moved", Toast.LENGTH_SHORT).show();
                                acc.updateInfo(context);
                                updateList();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Toast.makeText(AlbumViewActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);
            }
        });
    }

    public void updateList(){
        photoAdapter.notifyDataSetChanged();
        photoListView.setAdapter(photoAdapter);
    }


}
