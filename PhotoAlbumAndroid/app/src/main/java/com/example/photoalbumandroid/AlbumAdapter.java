package com.example.photoalbumandroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import model.Album;

public class AlbumAdapter extends ArrayAdapter<Album> {

    public AlbumAdapter(Context context,  ArrayList<Album> album) {
        super(context, R.layout.list_albums, album);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater albumInflater = LayoutInflater.from(getContext());
        View customView = albumInflater.inflate(R.layout.list_albums, parent, false);

        String album_name = getItem(position).getName();
        TextView album_text = (TextView) customView.findViewById(R.id.album_text);

        album_text.setText(album_name);

        return customView;
    }
}
