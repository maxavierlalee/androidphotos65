package com.example.photoalbumandroid;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import model.Account;
import model.Album;
import model.Tag;

public class PhotoViewActivity extends AppCompatActivity {
    public static Account acc;
    private int selectedIndex = AlbumViewActivity.selectedIndex;

    List<String> spinnerArray =  new ArrayList<String>();
    public ArrayAdapter<String> adapter;

//    ListView tagPersonListView;
//    List<String> personArray;


    public TagAdapter tagAdapter;
    public ListView personListViewTags;
    public int selectedTagIndex;
    public Album currAlb;


    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);
        this.acc = HomeActivity.acc;


        currAlb = acc.albums.get(HomeActivity.selectedAlbumIndex);

        // test tags
//        currAlb.getPhotos().get(selectedIndex).tags.add(new Tag("Person", "Henry"));
//        currAlb.getPhotos().get(selectedIndex).tags.add(new Tag("Location", "Henry2"));

        TextView photoName = (TextView) findViewById(R.id.photoName);
        final TextView photoLocation = (TextView) findViewById(R.id.photoLocation);
        TextView photoCaption = (TextView) findViewById(R.id.photoCaption);
        if (currAlb.getPhotos().get(selectedIndex).title != null) {
            photoName.setText(currAlb.getPhotos().get(selectedIndex).title);
        }
        if (currAlb.getPhotos().get(selectedIndex).caption != null) {
            photoCaption.setText(currAlb.getPhotos().get(selectedIndex).caption);
        }
        for (Tag t : currAlb.getPhotos().get(selectedIndex).tags) {
            if (t.getTagName().equalsIgnoreCase("Location")) {
                photoLocation.setText(t.getTagValue());
            }
        }

        tagAdapter = new TagAdapter(this, currAlb.getPhotos().get(selectedIndex).tags);
        personListViewTags = (ListView) findViewById(R.id.personListViewTags);
        personListViewTags.setAdapter(tagAdapter);

        final EditText tagValueTextInput = (EditText) findViewById(R.id.tagValue);
        Button addEditButton = (Button) findViewById(R.id.addOrEditButton);
        Button deleteButton = (Button) findViewById(R.id.deleteTagButton);
        Button leftButton = (Button) findViewById(R.id.leftButton);
        Button rightButton = (Button) findViewById(R.id.rightButton);
        ImageView img = (ImageView) findViewById(R.id.selectedPhoto);
        int draw = context.getResources().getIdentifier(currAlb.getPhotos().get(selectedIndex).path, "drawable", context.getPackageName());
        img.setImageResource(draw);

        final Spinner tagNameSpinner = (Spinner) findViewById(R.id.tagSpinner);
        spinnerArray.clear();
        spinnerArray.add("Person");
        spinnerArray.add("Location");
        adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tagNameSpinner.setAdapter(adapter);

//        if(personArray == null){
//            System.out.println("Found null");
//            personArray =  new ArrayList<String>();
//        }
//        tagAdapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_list_item_1,
//                personArray);
//        tagPersonListView = (ListView) findViewById(R.id.personListViewTags);
//        tagPersonListView.setAdapter(tagAdapter);


        addEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("add detected");
                if (tagValueTextInput.getText().toString().isEmpty()) {

                } else {
                    String value = tagValueTextInput.getText().toString();
                    String name = tagNameSpinner.getSelectedItem().toString();
                    if (name.equalsIgnoreCase("Person")) {
                        if (!tagExists(name, value)) {
                            currAlb.getPhotos().get(selectedIndex).tags.add(new Tag("Person", value));
                            tagAdapter.notifyDataSetChanged();
                            acc.updateInfo(context);
                            // System.out.println("tags: " + personArray.toString());
                        }
                    } else if (name.equalsIgnoreCase("Location")) {
                        photoLocation.setText(value);
                        if (tagLocationExists(name, value)) {
                            for (Tag t : currAlb.getPhotos().get(selectedIndex).tags) {
                                if (t.getTagName().equalsIgnoreCase("Location")) {
                                    t.tagValue = value;
                                    tagAdapter.notifyDataSetChanged();
                                    acc.updateInfo(context);
                                    break;
                                }
                            }
                        } else {
                            currAlb.getPhotos().get(selectedIndex).tags.add(new Tag(name, value));
                            tagAdapter.notifyDataSetChanged();
                            acc.updateInfo(context);
                        }

                    }
                }
            }
        });

        personListViewTags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int index, long id) {
                Tag t = (Tag) parent.getItemAtPosition(index);
                selectedTagIndex = index;
                String value = t.getTagValue();
                Toast.makeText(PhotoViewActivity.this, value, Toast.LENGTH_SHORT).show();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(selectedTagIndex);
                if (selectedTagIndex > -1) {
                    try {
                        Tag t = currAlb.getPhotos().get(selectedIndex).tags.get(selectedTagIndex);
                        currAlb.getPhotos().get(selectedIndex).tags.remove(t);
                        Toast.makeText(PhotoViewActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(PhotoViewActivity.this, "Cannot Delete", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PhotoViewActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    return;
                }
                acc.updateInfo(context);
                tagAdapter.notifyDataSetChanged();
            }
        });

        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AlbumViewActivity.selectedIndex>0){
                    AlbumViewActivity.selectedIndex--;
                    Intent intent = new Intent(getApplicationContext(), PhotoViewActivity.class);
                    startActivity(intent);
                }
            }
        });
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AlbumViewActivity.selectedIndex<currAlb.getPhotos().size()-1) {
                    AlbumViewActivity.selectedIndex++;
                    Intent intent = new Intent(getApplicationContext(), PhotoViewActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

        public boolean tagExists (String name, String value){
            for (Tag t : currAlb.getPhotos().get(selectedIndex).tags) {
                if (t.getTagName().equalsIgnoreCase(name) && t.getTagValue().equalsIgnoreCase(value)) {
                    return true;
                }
            }
            return false;
        }
        public boolean tagLocationExists (String name, String value){
            for (Tag t : currAlb.getPhotos().get(selectedIndex).tags) {
                if (t.getTagName().equalsIgnoreCase(name)) {
                    return true;
                }
            }
            return false;
        }
    }

