package com.example.photoalbumandroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import model.Album;
import model.Tag;

public class TagAdapter extends ArrayAdapter<Tag> {

    public TagAdapter(Context context, ArrayList<Tag> tags) {
        super(context, R.layout.list_tags, tags);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater albumInflater = LayoutInflater.from(getContext());
        View customView = albumInflater.inflate(R.layout.list_tags, parent, false);

            String tag_name = getItem(position).getTagName();
            TextView tag_text = (TextView) customView.findViewById(R.id.tag_text);
            tag_text.setText(tag_name);

            String tag_value = getItem(position).getTagValue();
            TextView value_text = (TextView) customView.findViewById(R.id.tag_value);

            value_text.setText(tag_value);

        return customView;
    }
}
