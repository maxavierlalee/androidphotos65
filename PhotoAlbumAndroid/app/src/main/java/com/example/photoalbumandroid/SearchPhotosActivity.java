package com.example.photoalbumandroid;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.Account;
import model.Album;
import model.Photo;
import model.Tag;

public class SearchPhotosActivity extends AppCompatActivity {
    public Account acc;
    final Context context = this;
    public AlbumAdapter tagAdapter;
    private int selectedPersonIndex;
    private int selectedLocationIndex;

    ListView tagPersonListView;
    ListView tagLocationListView;
    List<String> spinnerArray =  new ArrayList<String>();
    List<String> personArray;
    List<String> locationArray;

    public ArrayList<Photo> uniquePhotos = new ArrayList<>();
    ListView photosListView;
    public PhotoAdapter photoAdapter;

    public ArrayAdapter<String> adapter;
    public ArrayAdapter<String> personAdapter;
    public ArrayAdapter<String> locationAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_photos);
        //this.acc = HomeActivity.acc;
        this.acc = HomeActivity.acc;

        final EditText tagValueTextInput = (EditText) findViewById(R.id.tagValueTextInput);
        Button addButton = (Button) findViewById(R.id.addButton);
        Button deleteButton = (Button) findViewById(R.id.deleteButton);
        Button filterButton = (Button) findViewById(R.id.filterButton);
        Button viewButton = (Button) findViewById(R.id.viewButton);

        final Spinner tagNameSpinner = (Spinner) findViewById(R.id.tagNameSpinner);
        spinnerArray.clear();
        spinnerArray.add("Person");
        spinnerArray.add("Location");
        adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tagNameSpinner.setAdapter(adapter);

        if(personArray == null){
            System.out.println("Found null");
            personArray =  new ArrayList<String>();
        }
        personAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                personArray);
        tagPersonListView = (ListView) findViewById(R.id.personListView);
        tagPersonListView.setAdapter(personAdapter);

        if(locationArray == null){
            System.out.println("Found null");
            locationArray =  new ArrayList<String>();
        }
        locationAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                locationArray);
        tagLocationListView = (ListView) findViewById(R.id.locationListView);
        tagLocationListView.setAdapter(locationAdapter);

        photoAdapter = new PhotoAdapter(this, uniquePhotos);
        photosListView = (ListView) findViewById(R.id.photosListView);
        photosListView.setAdapter(photoAdapter);


        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("add detected");
                if(tagValueTextInput.getText().toString().isEmpty()){

                } else{
                    String value = tagValueTextInput.getText().toString();
                    String name = tagNameSpinner.getSelectedItem().toString();
                    if(name.equalsIgnoreCase("Person")){
                        if(!personArray.contains(value)){
                            personArray.add(value);
                            personAdapter.notifyDataSetChanged();
                            System.out.println("tags: "+personArray.toString());
                        }
                    } else if(name.equalsIgnoreCase("Location")){
                        if(!locationArray.contains(value)){
                            locationArray.add(value);
                            locationAdapter.notifyDataSetChanged();
                            System.out.println("tags: "+locationArray.toString());
                            System.out.println("adding... now array: "+locationArray);
                        }
                    }
                }
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("delete detected");
                if (selectedPersonIndex > -1) {
                    try {
                        personArray.remove(selectedPersonIndex);
                        Toast.makeText(SearchPhotosActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(SearchPhotosActivity.this, "Cannot Delete", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SearchPhotosActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selectedLocationIndex > -1) {
                    try {
                        locationArray.remove(selectedLocationIndex);
                        Toast.makeText(SearchPhotosActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(SearchPhotosActivity.this, "Cannot Delete", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SearchPhotosActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    return;
                }
                personAdapter.notifyDataSetChanged();
                locationAdapter.notifyDataSetChanged();
            }
        });
        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("view detected");

            }
        });
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uniquePhotos.clear();
                photoAdapter.notifyDataSetChanged();

                System.out.println("filter detected");
                System.out.println("Creating a list of all unique photos");
                for(Album al : acc.getAlbums()){
                    for(Photo ph : al.getPhotos()){
                        if(!uniquePhotos.contains(ph)){
                            uniquePhotos.add(ph);
                        }
                    }
                }
                System.out.println("Unique Photos: "+uniquePhotos.toString());


                System.out.println("Filtering photos by person/location tags");
                Iterator<Photo> i = uniquePhotos.iterator();
                while (i.hasNext()) {
                    Photo ph = i.next();
                    if(photoMatchesFilter(ph)){
                        // keep photo
                    } else{
                        i.remove();
                    }
                }

                if(!uniquePhotos.isEmpty()){
                    System.out.println("Rendering photos onto list");
                    photoAdapter.notifyDataSetChanged();
                } else{
                    System.out.println("No photos by that filter");
                }
            }
        });
    }

    public boolean photoMatchesFilter(Photo ph){
        if(personArray != null){
            System.out.println("person");
            for(String val : personArray){
                System.out.println("person 21312");
                if(tagExists("Person", val, ph)){
                    System.out.println("match: "+val);
                    return true;
                }
            }
        }
        if(locationArray != null){
            System.out.println("loc");
            for(String val : locationArray){
                System.out.println("loc 21312");
                if(tagExists("Location", val, ph)){
                    System.out.println("match: "+val);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean tagExists (String name, String value, Photo ph){
        for (Tag t : ph.tags) {
            if (t.getTagName().equalsIgnoreCase(name) && t.getTagValue().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }

}


