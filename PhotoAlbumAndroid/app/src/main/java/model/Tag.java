package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This object is for tagging photos and giving them specific details
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class Tag implements Serializable {
    private static final long serialVersionUID = 4776274829799049754L;
    public String tagName = "";
    public String tagValue = "";

    public Tag(String tagName){
        this.tagName = tagName;
    }
    public Tag(String tagName, String tagValue){
        this.tagName = tagName;
        this.tagValue = tagValue;
    }

    /**
     * Adds a value to the tag
     * @param value
     */
    public void addValue(String value){
        this.tagValue = value;
    }


    /**
     * Determines if a tag is equal to another.
     * @param tag
     * @return
     */
    public boolean equals(Tag tag){
        return tag.tagName.equals(tagName) && tag.tagValue.equals(tagValue);
    }

    /**
     * @return name of the tag
     */
    public String getTagName(){
        return this.tagName;
    }

    /**
     * @return the string value of the tag
     */
    public String getTagValue(){
        return this.tagValue;
    }


    public String toString(){
        return this.tagName+": "+this.tagValue;
    }
}
