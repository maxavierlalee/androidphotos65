package model;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * This method is the central that accounts refer to, holds getters and setters that refer to the specific user.
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class Account implements Serializable {
    private String username;
    public ArrayList<Album> albums;
    private List<String> customTagNames = new ArrayList<>();
    private static final long serialVersionUID = 1784709278836007561L;

    /**
     * All an account needs to be created is a given name
     * @param username
     */
    public Account(String username){
        this.username = username;
        this.customTagNames.add("Person");
        this.customTagNames.add("Location");
    }

    /**
     * Sets the username of the account.
     * @param username
     */
    public void setUsername(String username){
        this.username=username;
    }

    /**
     * return what the user's username is.
     * @return
     */
    public String getUsername(){
        return this.username;
    }

    /**
     * Allows the user to be expressed through a String
     * @return
     */
    public String toString() {
        return this.username;
    }

    /**
     * returns a list of albums that the user holds
     * @return
     */
    public List<Album> getAlbums() {
        return this.albums;
    }

    /**
     * Adds an album object to the list of albums that a user holds.
     * @param album
     */
    public void addAlbum(Album album){
        this.albums.add(album);
        //AdminController.updateInfo();
        Log.d("MSG", "UPDATEDDDDDDDDDDDDDDDDDDDDDDDD");
    }

    /**
     * Deletes a specific album from the list of albums that a user holds.
     * @param album
     */
    public void deleteAlbum(Album album){
        this.albums.remove(album);
    }

    /**
     * Returns true if an ablum already exists within the list of albums that a user holds.
     * @param name
     * @return boolean
     */
    public boolean albumExists(String name){
        for(Album al : this.albums){
            if(al.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a custom tag to a specific photo if the tag does not already exist.
     * @param tag
     */
    public void addCustomTag(Tag tag){
        if(!customTagNameExists(tag.getTagName())){
            System.out.println("Custom tag does not exist, adding");
            this.customTagNames.add(tag.getTagName());
        } else{
            System.out.println("Custom tag already exists");
        }
    }

    /**
     * Removes a specific tag of a photo.
     * @param tag
     */
    public void removeCustomTag(Tag tag) {
        this.customTagNames.remove(tag.getTagName());
    }

    /**
     * Returns the custom tag that is applied towards a photo
     * @return List
     */
    public List<String> getCustomTagNames(){
        return this.customTagNames;
    }

    /**
     * returns true if a tag already exists and is applied to a specific photo.
     * @param tagName
     * @return
     */
    public boolean customTagNameExists(String tagName){
        for(String name : customTagNames){
            if(tagName.equals(name)){
                System.out.println("Match was found for custom tag");
                return true;
            }
        }
        return false;
    }

    public static Account getSavedData (Context context){
        Account acc = null;
        try {
            FileInputStream fis = context.openFileInput("account.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            acc = (Account) ois.readObject();

            if (acc.albums == null) {
                acc.albums = new ArrayList<Album>();
            }
            fis.close();
            ois.close();
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        } catch (ClassNotFoundException e) {
            return null;
        } catch (Exception e) {
            return null;
        }
        return acc;
    }

    public void updateInfo(Context context){
        Log.d("MSG", "ATTEMPTTTTTTTTTTTTTTT");
        try {
            //FileOutputStream fileOutputStream = new FileOutputStream("java/info/info.dat");
            FileOutputStream fileOutputStream = context.openFileOutput("account.ser", Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(this);
            objectOutputStream.close();
            fileOutputStream.close();
            System.out.println("success on update");
            Log.d("MSG", "UPDATEDDDDDDDDDDDDDDDDDDDDDDDD");
        } catch (IOException exception) {
            System.out.println("failed to update");
            Log.d("MSG", "NOOOOOOOOOOOOOOOOOOOO");
            exception.printStackTrace();
        }
    }
}
