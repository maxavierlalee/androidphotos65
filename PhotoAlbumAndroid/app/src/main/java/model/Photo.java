package model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Photo object that holds parameters that make a photo unique.
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class Photo implements Serializable {
    private static final long serialVersionUID = 2574633947395074758L;

    public ArrayList<Tag> tags;
    public String title;
    public String path;
    public String caption;
    transient Bitmap image;

    public Photo(){

    }
    public Photo(String title, String path) {
        this.title = title;
        this.caption = "";
        this.path = path;
        this.tags = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public String getCaption() {
        return caption;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Bitmap getImage() {
        return image;
    }
    public void setImage(Bitmap image) {
        this.image = image;
    }

//    public boolean equals(Photo otherPhoto) {
//        return this.title.equals(otherPhoto.title) && this.img.equals(otherPhoto.img);
//    }

    public static Comparator<Photo> compareName() {
        return new Comparator<Photo>(){
            @Override
            public int compare(Photo arg0, Photo arg1){
                return arg0.title.compareTo(arg1.title);
            }
        };
    }

    public boolean equals(Object o){
        if(o == null || !(o instanceof Photo))
            return false;
        Photo p = (Photo) o;
        return p.path.equals(this.path) && p.title.equals(this.title);
    }

    public boolean tagExists(String tagName){
        for(Tag t : tags){
            if(t.getTagName().equals(tagName)){
                return true;
            }
        }
        return false;
    }
    public String toString(){
        return title;
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        int b;
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        while((b = ois.read()) != -1)
            byteStream.write(b);
        byte bitmapBytes[] = byteStream.toByteArray();
        image = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        if(image != null){
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 0, byteStream);
            byte bitmapBytes[] = byteStream.toByteArray();
            oos.write(bitmapBytes, 0, bitmapBytes.length);
        }
    }

    public boolean tagValueAlreadyExists(String tagValue){
        for(Tag t : tags){
            if(t.tagValue.equalsIgnoreCase(tagValue)){
                return true;
            }
        }
        return false;
    }
}
